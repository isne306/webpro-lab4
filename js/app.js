function check(x)
{
    var forename = x.Forename.value;
    var email = x.Email.value;
	var age = (new Date).getFullYear()-x.Age.value.split("-")[0];
    var password = x.Password.value;
	var username = x.Username.value;
	var surname = x.Surname.value;
	var repassword = x.rePassword.value;
	
	document.getElementById('fn').innerHTML="";
	document.getElementById('sn').innerHTML="";
	document.getElementById('us').innerHTML="";
	document.getElementById('ps').innerHTML="";
	document.getElementById('pr').innerHTML="";
    document.getElementById('ag').innerHTML="";
    document.getElementById('em').innerHTML="";


	//forename check
    if (forename == null || forename == "")
    {
        alert("Forename must not be blank.");
        document.getElementById('fn').innerHTML="*";
		return false;
    }
    if (!forename.match(/^[a-zA-Z]+$/))
    {
        alert("Forename must be alphabet characters.");
        document.getElementById('fn').innerHTML="*";
		return false;
    }
	if (forename.length < 3)
    {
        alert("Forename must have at least 3 characters.");
        document.getElementById('fn').innerHTML="*";
		return false;
    }
	if (forename.indexOf(' ') != -1 )
    {
        alert("Forename must not contain spaces.");
        document.getElementById('fn').innerHTML="*";
		return false;
    }
	
  //surname check
        if (surname == null || surname == "")
    {
        alert("Please enter your surname.");
		document.getElementById('sn').innerHTML="*";
		return false;
    }
	if (!surname.match(/^[a-zA-Z]+$/))
    {
        alert("Surname must be alphabet characters.");
		document.getElementById('sn').innerHTML="*";
        return false;
    }
	if (surname.length < 3)
    {
        alert("Surname must have at least 3 characters.");
		document.getElementById('sn').innerHTML="*";
        return false;
    }
	if (surname.indexOf(' ') != -1 )
    {
        alert("Surname must not contain spaces.");
		document.getElementById('sn').innerHTML="*";
        return false;
    }
    //username check
    if (username == null || username == "")
    {
        alert("Please enter your  username.");
        document.getElementById('us').innerHTML="*";
        return false;
    }
    if (!username.match(/^[a-zA-Z0-9\_\-]{5,}$/))
    {
        alert("Username can be alphabet characters,number,_ and - .");
        document.getElementById('us').innerHTML="*";
        return false;
    }
	    if (username.length < 5)
    {
        alert("Username must have at least 5 characters.");
        document.getElementById('us').innerHTML="*";
        return false;
    }

	//pass check
	    if (password == null || password == "")
    {
        alert("Please enter your password.");
        document.getElementById('ps').innerHTML="*";
        return false;
    }
	 if (password != repassword )
    {
        alert("Password are not the same.");
        document.getElementById('ps').innerHTML="*";
        document.getElementById('pr').innerHTML="*";
		return false;
    }
    if (!password.match(/[A-Z]+/g))
    {
        alert("Password must be contain Upper alphabet.");
        document.getElementById('ps').innerHTML="*";
		return false;
    }
    if (!password.match(/[a-z]+/g))
    {
        alert("Password must be contain Lower alphabet.");
        document.getElementById('ps').innerHTML="*";
        return false;
    }
    if (!password.match(/[0-9]+/g))
    {
        alert("Password must be contain Numbers.");	
        document.getElementById('ps').innerHTML="*";
        return false;
    }
    if (!password.match(/[\!\@\#\$\%\^\&\*\(\)\_\+]+/g))
    {
        alert("Password must be contain Symbols.");
        document.getElementById('ps').innerHTML="*";
        return false;
    }

	if (x.Password.value.length < 8)
    {
        alert("Password must have at least 8 characters.");
        document.getElementById('ps').innerHTML="*";
        return false;
    }
	//pass check
	    if (repassword == null || repassword == "")
    {
        alert("Please enter your password.");
        document.getElementById('pr').innerHTML="*";
		return false;
    }
    if (!repassword.match(/[A-Z]+/g))
    {
        alert("Password must be contain Upper alphabet.");
        document.getElementById('pr').innerHTML="*";
		return false;
    }
    if (!repassword.match(/[a-z]+/g))
    {
        alert("Password must be contain Lower alphabet.");
        document.getElementById('pr').innerHTML="*";
		return false;
    }
    if (!repassword.match(/[0-9]+/g))
    {
		document.getElementById('pr').innerHTML="*";
        alert("Password must be contain Numbers.");
        return false;
    }
    if (!repassword.match(/[\!\@\#\$\%\^\&\*\(\)\_\+]+/g))
    {
		document.getElementById('pr').innerHTML="*";
        alert("Password must be contain Symbols.");
        return false;
    }

	if (x.rePassword.value.length < 8)
    {
		document.getElementById('pr').innerHTML="*";
        alert("Password must have at least 8 characters.");
        return false;
    }
	//age check
      $(document).ready(function()
    {
        $("#datepicker").datepicker({format: 'mm/dd/yyyy'});
        });
        if (age == null || age == "")
        {
		    document.getElementById('ag').innerHTML="*";
            alert("Please enter your age.");
            return false;
        }
        if(age < 18 || age > 110)
        {
		    document.getElementById('ag').innerHTML="*";
            alert("Age is not valid.");
            return false;
    }
    //mail check
    
    if (email == null || email == "")
    {
		document.getElementById('em').innerHTML="*";
        alert("Please enter your e-mail.");
        return false;
    }
    if (!email.match(/[a-zA-Z0-9\_\-\.]+\@([a-zA-Z0-9\-]{3,})\.([a-z]{2,3})(\.([a-z]{2}))?/g))
    {
		document.getElementById('em').innerHTML="*";
        alert("Email must be in pattern 'abc@def.com'.");
        return false;
    }
    return true;

}